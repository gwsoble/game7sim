﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbiters : MonoBehaviour
{
	public GameObject attractor;
	public GameObject velocityHandle;
	public int sphereCount = 500;
	public int maxRadius = 200;
	public GameObject[] spheres;
	public Material[] mats;
	public Material trailMat;

	private void Awake()
	{
		spheres = new GameObject[sphereCount];
	}
	// Start is called before the first frame update
	void Start()
    {
		spheres = CreateSpheres(sphereCount, maxRadius);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public GameObject[] CreateSpheres(int count, int radius)
	{
		GameObject[] sphs = new GameObject[count];
		GameObject sphereToCopy = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		Rigidbody rb = sphereToCopy.AddComponent<Rigidbody>();
		rb.useGravity = false;
		sphereToCopy.AddComponent<SimpleKeplerOrbits.KeplerOrbitMover>();
		sphereToCopy.AddComponent <SimpleKeplerOrbits.KeplerOrbitLineDisplay>();
		gameObject.GetComponent<SimpleKeplerOrbits.KeplerOrbitMover>().AttractorSettings.AttractorObject = attractor.transform;
		gameObject.GetComponent<SimpleKeplerOrbits.KeplerOrbitMover>().AttractorSettings.AttractorMass = 1000;
		gameObject.GetComponent<SimpleKeplerOrbits.KeplerOrbitMover>().AttractorSettings.GravityConstant = 0.1f;


		for (int i = 0; i < count; i++)
		{
			GameObject sp = GameObject.Instantiate(sphereToCopy);
			sp.transform.position = this.transform.position +
									new Vector3(Random.Range(-maxRadius, maxRadius),
												Random.Range(-10, 10),
												Random.Range(-maxRadius, maxRadius));
			sp.transform.localScale *= Random.Range(0.5f, 1);
			TrailRenderer tr = sp.AddComponent<TrailRenderer>();
			tr.time = 1.0f;
			tr.startWidth = 0.1f;
			tr.endWidth = 0;
			//tr.material = trailMat;
			tr.startColor = new Color(1, 1, 0, 0.1f);
			tr.endColor = new Color(0, 0, 0, 0);
			spheres[i] = sp;
		}

		GameObject.Destroy(sphereToCopy);
		return spheres;
	}
}
