﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit : MonoBehaviour
{
	public int sphereCount = 10;
	public int maxRadius = 50;
	public GameObject[] spheres;
	public Material[] mats;
	public Material trailMait;
	const float G = 6.67f;

	private void Awake()
	{
		spheres = new GameObject[sphereCount];
	}
	// Start is called before the first frame update
	void Start()
    {
		spheres = CreateSpheres(sphereCount, maxRadius);
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject s in spheres)
		{
			Vector3 difference = this.transform.position - s.transform.position;
			float dist = difference.magnitude;
			Vector3 gravityDirection = difference.normalized;
			float gravity = 10 * (this.transform.localScale.x * s.transform.localScale.x * 80) / (dist * dist);
			Vector3 gravityVector = (gravityDirection * gravity);
			s.transform.GetComponent<Rigidbody>().drag = 0;
			s.transform.GetComponent<Rigidbody>().AddForce(s.transform.forward, ForceMode.Acceleration);
			s.transform.GetComponent<Rigidbody>().AddForce(gravityVector, ForceMode.Acceleration);
		}
    }

	public GameObject[] CreateSpheres(int count, int radius)
	{
		GameObject[] sphs = new GameObject[count];
		GameObject sphereToCopy = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		Rigidbody rb = sphereToCopy.AddComponent<Rigidbody>();
		rb.useGravity = false;

		for (int i = 0; i < count; i++)
		{
			GameObject sp = GameObject.Instantiate(sphereToCopy);
			sp.transform.position = this.transform.position +
									new Vector3(Random.Range(-maxRadius, maxRadius),
												Random.Range(-10, 10),
												Random.Range(-maxRadius, maxRadius));
			sp.transform.localScale *= Random.Range(0.5f, 3);
			sp.GetComponent<Renderer>().material = mats[Random.Range(0, mats.Length)];
			TrailRenderer tr = sp.AddComponent<TrailRenderer>();
			tr.time = 1.0f;
			tr.startWidth = 0.1f;
			tr.endWidth = 0;
			//tr.material = trailMat;
			tr.startColor = new Color(1, 1, 0, 0.1f);
			tr.endColor = new Color(0, 0, 0, 0);
			spheres[i] = sp;
		}

		GameObject.Destroy(sphereToCopy);
		return spheres;
	}
}
