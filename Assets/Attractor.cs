﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attractor : MonoBehaviour
{
	public GameObject body;
	public GameObject[] spheres;
	public int sphereCount = 1000;
	public int maxRadius = 200;
	const float G = 6.674f;
	public Rigidbody rb;
	// Start is called before the first frame update
	public static List<Attractor> Attractors;
	private void FixedUpdate()
	{
		foreach (Attractor attractor in Attractors)
		{
			if (attractor != this)
				Attract(attractor);
		}
	}

	private void Awake()
	{
		spheres = new GameObject[sphereCount];
	}
	private void Start()
	{
		spheres = CreateSpheres(sphereCount, maxRadius);
	}
	void OnEnable()
	{
		if (Attractors == null)
			Attractors = new List<Attractor>();
		Attractors.Add(this);
	}

	private void OnDisable()
	{
		Attractors.Remove(this);	
	}
	void Attract(Attractor objectToAttract)
	{
		Rigidbody rbToAttract = objectToAttract.rb;
		Vector3 direction = rb.position - rbToAttract.position;
		float distance = direction.magnitude;

		float forceMagnitude = G *  (rb.mass * rbToAttract.mass) / Mathf.Pow(distance, 2);
		Vector3 force = direction.normalized * forceMagnitude;

		rbToAttract.AddForce(force);
	}

	public GameObject[] CreateSpheres(int count, int radius)
	{
		GameObject[] sphs = new GameObject[count];
		for (int i = 0; i < count; i ++)
		{
			GameObject sp = Instantiate(body);
			sp.transform.position = this.transform.position +
									new Vector3(Random.Range(-maxRadius, maxRadius),
												Random.Range(-10, 10),
												Random.Range(-maxRadius, maxRadius));
			spheres[i] = sp;
		}

		return spheres;
	}
}
